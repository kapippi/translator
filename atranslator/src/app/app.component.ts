import { Component, ElementRef } from '@angular/core';
import { GaDeRyPoLuKi } from './gaderypoluki/gaderypoluki';

@Component({
  selector: 'app-root',
  template: `
    <div class="form">
      <div class="input">
        <span>Do translacji: </span><input #input>
        <button (click)="onClick(input.value)">translate</button>
      </div>
      <div class="input">
        <span>Wynik translacji: </span><span>{{translated}}</span>
      </div>
    </div>
  `,
  styles: [`
    .form {
      display: flex;
      flex-direction: column;
    }

    .input {
      display: flex;
      flex-direction: row;
    }
  `]
})
export class AppComponent {

  translated: string;

  constructor(private translator: GaDeRyPoLuKi) {

  }

  onClick(value: any): void {
    this.translated = this.translator.translate(value);
  }

}
